import express from 'express';
import mongoose from 'mongoose';
import * as config from './globals/config';

const app = express();
mongoose.Promise = global.Promise;
import accounts from './accounts/routes';
import pictures from './pictures/routes';

app.use('/api', pictures, accounts);

let server = app.listen(8080, () => {
  console.log('SERVER IS LISTENING TO ', server.address().port);
});
