import {Router} from 'express';
import {loginController, signupController} from './controllers';

const accounts = new Router();

accounts.post('/login', loginController);
accounts.post('/register', signupController);

export default accounts;
